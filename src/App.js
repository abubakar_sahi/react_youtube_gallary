import React, { Component } from 'react';
import SearchBar from './components/search_bar';
import VideoList from './components/video_list';
import VideoDetail from './components/video_detail';
import SearchYoutube from 'youtube-api-search'
import logo from './logo.svg';
import _ from 'lodash'
import './App.css';

class App extends Component {
  APP_KEY = "AIzaSyDEhkxBIIU8mO6us_XL6Xfss9RX6Kd_tC0";
  constructor(props){
    super(props);
    this.state = {
      videos:[] ,
      selectedVideo : null
    };
    this.videoSearch("react tutorials");
  }

  render() {
    const videoSearch = _.debounce(term=>this.videoSearch(term),300);
    return (
      <div className="container">
        <SearchBar onChangeSearchTerm={videoSearch} />
        <div className="row">
            <div className="col-md-8">
              <VideoDetail video={this.state.selectedVideo} />
            </div>
            <div className="col-md-4">
              <VideoList onVideoSelect={selectedVideo => this.setState({selectedVideo})} videos={this.state.videos} />
            </div>
        </div>
      </div>
    );
  }

  videoSearch(search_term){
    SearchYoutube({key:this.APP_KEY, term:search_term}, (videos)=>{
        this.setState({
            videos:videos,
            selectedVideo:videos[0]
          });
    });
  }
}

export default App;
