import React, {Component} from 'react';

class  SearchBar extends Component{

    constructor(props){
        super(props);

        this.state = {search_term : ''};
    }
    render(){
        return (
            <div className="search-bar">
                <input value={this.state.search_bar} onChange={event=>this.onChangeSearchBar(event.target.value)} name="search_bar" className="formControlls" />
            </div>

        );
    }

    onChangeSearchBar(term){
        this.setState({search_term : term});
        this.props.onChangeSearchTerm(term);
    }
};

export default SearchBar;
