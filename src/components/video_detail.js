import React from 'react'

const VideoDetail = ({video})=>{
    if(!video){
      return (<div>Loading...</div>);
    }
    const videoId = video.id.videoId;
    const url = `https://www.youtube.com/embed/${video.id.videoId}`;

    return (
      <div className="video-detail">
        <div className="embed-responsive embed-responsive-16by9">
        <iframe width="420" height="315" src={url}>
        </iframe>
        </div>
        <div className="details">
          <div>{video.snippet.title}</div>
          <div>{video.snippet.description}</div>
        </div>
      </div>
    );
};

export default VideoDetail;
